import React from 'react';
import './Licence.scss'

const Licence = () => {
  return (
    <section className='lic flex-center'>
        © 2022 ASCL . All rights reserved.
    </section>
  )
}

export default Licence
