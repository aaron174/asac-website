import React from 'react';
import { Countdown } from '../Countdown/Countdown';
import Mintbox from '../Mintbox/Mintbox';
import MintHeader from '../MintHeader/MintHeader';
import './Mint.scss';

// <div id='semi-opaque' className="container flex-between aos-init aos-animate">

const Mint = () => {
  return (
    <section id='mint' className='mintS'>
    <div id="background">
    <MintHeader />
    <div className="timer flex-center">
    <Countdown  />
    </div>
    <Mintbox />
    </div>
    </section>
  )
}

export default Mint
